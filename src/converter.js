/**
 * Padding outputs 2 characters always
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
}

module.exports = {
    /**
     * Converts RGB to Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    rgbToHex: (red, green, blue) => {
        //console.log({red, green, blue});

        const redHex = red.toString(16); // 0-255 -> 0-ff
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);

        const hex = pad(redHex) + pad(greenHex) + pad(blueHex);
        //console.log({hex});

        return pad(redHex) + pad(greenHex) + pad(blueHex); // hex string 6 characters
    },

    /**
     * Converts Hex string to RGB
     * @param {string} hex 000000-ffffff
     * @returns {string} 0-255 rgb values separated by commas
     */
    hexToRgb: (hex) => {
        const r = parseInt(hex.slice(0,2), 16);
        const g = parseInt(hex.slice(2,4), 16);
        const b = parseInt(hex.slice(4,6), 16);

        return `${r}, ${g}, ${b}`;
    }
}